# Wiki Workshop 2025 Templates Extended Abstract

This repository contains the format templates for submissions of extended abstracts for [Wiki Workshop 2025](https://meta.wikimedia.org/wiki/Wiki_Workshop_2025). The detailed submission instructions can be found [here](https://meta.wikimedia.org/wiki/Wiki_Workshop_2025/Call_for_Papers#Submission_Instructions).

## Usage

We provide templates for LaTeX and Word. You can access the templates in the following ways.

LaTex (recommended):  
- Link to [Overleaf](https://www.overleaf.com/read/hbpgthkcqznx#66d1ed). You can make a copy of the project or download the source files.
- Download the zip-file from the source files in this repository.

Word:  
- Link to [Google Docs](https://docs.google.com/document/d/1Hgfa7tlutjFVOSB-P6MMeS08Sbx71M3kJZ3j-urtZnk/edit). You can make a copy of the document and edit directly or download the template as a docx- or odt-file.
- Download the docx-file from the source files in this repository.

## License
This work is licensed under a Creative Commons Attribution 4.0 International License [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

## Attribution
This template was adapted from the template for [SDSS Conference Extended Abstract](https://www.overleaf.com/latex/templates/sdss2023-latex/kwvxycfjytws) (Donna LaLonde, CC BY 4.0).

## Questions
For questions, email wikiworkshop@googlegroups.com with the tag [Research Track] in the subject of your email.
